import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalReservationComponent } from './historical-reservation.component';

describe('HistoricalReservationComponent', () => {
  let component: HistoricalReservationComponent;
  let fixture: ComponentFixture<HistoricalReservationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoricalReservationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HistoricalReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
