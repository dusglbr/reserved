import { Injectable } from '@angular/core';
import { Reservation } from '../../models/Reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private storedReservations: Reservation[];
  constructor() { }

  getReservations(): Reservation[] {
    let localRes = localStorage.getItem('reservations');
    if (localRes) {
      this.storedReservations = JSON.parse(localRes);
    }
    else this.storedReservations = [];

    return this.storedReservations;

  }

  addReservation(res: Reservation) {

    let localRes = localStorage.getItem('reservations');

    if (localRes) {
      this.storedReservations = JSON.parse(localRes);
    }

    //we have reservations in local storage
    if (this.storedReservations) {

      //this reservation already exists, so should just return
      let existingReservation = this.storedReservations.find(r => r.guid === res.guid);
      if (existingReservation) {

        return;
      }
      // we have reservations, but this one is new
      else {
        this.storedReservations.push(res);
      }

    }
    //no reservations in local storage, or valid ones, so let's start over
    else {
      this.storedReservations = [];
      this.storedReservations.push(res);
    }

    localStorage.setItem('reservations', JSON.stringify(this.storedReservations));
  }

  modifyReservation(res: Reservation) {

    let localRes = localStorage.getItem('reservations');

    if (localRes) {
      this.storedReservations = JSON.parse(localRes);
    }

    //we have reservations in local storage
    if (this.storedReservations) {

      //let's find it then modify it
      let existingReservation = this.storedReservations.find(r => r.guid === res.guid);
      if (existingReservation) {

        existingReservation.date = res.date;
        existingReservation.time = res.time;
        existingReservation.name = res.name;
        existingReservation.numPeople = res.numPeople;
        existingReservation.status = res.status;

      }

    }

    localStorage.setItem('reservations', JSON.stringify(this.storedReservations));

  }

  deleteReservation(res: Reservation) {

    let localRes = localStorage.getItem('reservations');

    if (localRes) {
      this.storedReservations = JSON.parse(localRes);
    }

    //we have reservations in local storage
    if (this.storedReservations) {

      //let's find it then modify it
      let index = this.storedReservations.findIndex(r => r.guid === res.guid);
      if (index > -1) {
        this.storedReservations.splice(index, 1);
      }

    }

    localStorage.setItem('reservations', JSON.stringify(this.storedReservations));
  }
}
