"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReservationStatus = void 0;
var ReservationStatus;
(function (ReservationStatus) {
    ReservationStatus[ReservationStatus["pending"] = 0] = "pending";
    ReservationStatus[ReservationStatus["complete"] = 1] = "complete";
    ReservationStatus[ReservationStatus["cancelled"] = 2] = "cancelled";
})(ReservationStatus = exports.ReservationStatus || (exports.ReservationStatus = {}));
//# sourceMappingURL=ReservationStatus.js.map