"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reservation = void 0;
var ReservationStatus_1 = require("./enums/ReservationStatus");
var uuid_1 = require("uuid");
var Reservation = /** @class */ (function () {
    function Reservation(data, status) {
        if (status === void 0) { status = ReservationStatus_1.ReservationStatus.pending; }
        this.guid = uuid_1.v4();
        this.date = data.date || '';
        this.time = data.time || '';
        this.name = data.name || '';
        this.numPeople = data.numPeople || 1;
        this.status = status;
    }
    return Reservation;
}());
exports.Reservation = Reservation;
//# sourceMappingURL=Reservation.js.map