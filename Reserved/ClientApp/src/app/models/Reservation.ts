import { ReservationStatus } from "./enums/ReservationStatus";
import { v4 as uuidv4 } from 'uuid';

export class Reservation {
  public guid: string;
  date: Date;
  time: string;
  name: string;
  numPeople: number;
  status: ReservationStatus


  constructor(data: any, status: ReservationStatus = ReservationStatus.pending) {
    this.guid = uuidv4();
    this.date = data.date || '';
    this.time = data.time || '';
    this.name = data.name || '';
    this.numPeople = data.numPeople || 1;
    this.status = status;
  }

}
