export enum ReservationStatus {
  pending,
  fulfilled,
  cancelled
}
