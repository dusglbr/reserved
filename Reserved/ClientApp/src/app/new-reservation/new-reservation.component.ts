import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Reservation } from '../models/Reservation';
import { ReservationService } from '../services/Reservation/reservation.service';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.css']
})
export class NewReservationComponent implements OnInit {
  reservationForm: FormGroup;

  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    this.reservationForm = new FormGroup({
      date: new FormControl('', Validators.required),
      time: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      numPeople: new FormControl('', [Validators.required, Validators.min(1)])
    });
  }

  onSubmit() {
    if (this.reservationForm.valid) {

      const reservation = new Reservation(this.reservationForm.value);
      this.reservationService.addReservation(reservation);
      location.reload();
    }

  }

}
