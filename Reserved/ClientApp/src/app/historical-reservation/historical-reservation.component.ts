import { Component, OnInit } from '@angular/core';
import { ReservationStatus } from '../models/enums/ReservationStatus';
import { Reservation } from '../models/Reservation';
import { ReservationService } from '../services/Reservation/reservation.service';

@Component({
  selector: 'app-historical-reservation',
  templateUrl: './historical-reservation.component.html',
  styleUrls: ['./historical-reservation.component.css']
})
export class HistoricalReservationComponent implements OnInit {

  public reservations: Reservation[];
  public selectedReservations: Reservation[];

  constructor(private reservationService: ReservationService) { }

  ngOnInit(): void {
    this.reservations = this.reservationService.getReservations();
    this.selectedReservations = [];
  }

  statusName(status: ReservationStatus) {
    return ReservationStatus[status];
  }

  displayTime(time: string) {

    let splitTime = time.split(':');

    let hours = Number(splitTime[0]);
    let minutes = Number(splitTime[1]);
    let formatedMinutes: string;
    if (minutes < 10) {
      formatedMinutes = "0" + minutes;
    }
    else {
      formatedMinutes = minutes.toString();
    }
    
    let isPM = hours > 11;
    if (isPM) {
      return (hours - 12) + ":" + formatedMinutes + " PM";

    }
    return hours + ":" + formatedMinutes + " AM";

  }

  completeReservation() {

    this.selectedReservations.forEach(res => {
      res.status = ReservationStatus.fulfilled;
      this.reservationService.modifyReservation(res);

    });

  }

  cancelReservation() {

    this.selectedReservations.forEach(res => {
      res.status = ReservationStatus.cancelled;
      this.reservationService.modifyReservation(res);

    });

  }

  deleteReservation() {
    this.selectedReservations.forEach(res => {
      res.status = ReservationStatus.cancelled;
      this.reservationService.deleteReservation(res);

      location.reload();
    });
  }

  selected(res: Reservation, e:any) {
    if (e.target.checked) {
      this.selectedReservations.push(res);
    }
    else {
      const index = this.selectedReservations.findIndex(x => x.guid === res.guid);
      if (index > -1)
      this.selectedReservations.splice(index, 1);
    }
    console.log(this.selectedReservations);
  }

}
